import sys
from Bio import SeqIO

"""
input RSEM.isoforms.results, trinity output
RSEM.isoforms.results columns:
0-transcript_id		1-gene_id	2-length	3-effective_length	
4-expected_count	5-TPM		6-FPKM		7-IsoPct

output filtered trinity output
picking the transcript with the highest IsoPct within a sub-component
If two transcripts have the same IsoPct, pick the longer one 
"""

def pick_isoform(block):
	if len(block) == 1: #only one transcript per gene
		return block[0][0]
	else:
		transcript_id,length,IsoPct = block[0][0],float(block[0][2]),float(block[0][7])
		for i in block[1:]:
			perc = float(i[7])
			if perc > IsoPct or (perc == IsoPct and float(i[2]) > length):
				transcript_id,length,IsoPct = i[0],float(i[2]),perc
		return transcript_id

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "python pick_isoform_trinity_RSEM.py RSEM.isoforms.results fasta_file"
		sys.exit(0)
		
	infile = open(sys.argv[1],"r")
	header = True
	last_gene = ""
	block = [] #store all lines of one gene
	isoform_picked = [] #store transcript_id to output
	print "Reading RSEM output"
	for line in infile:
		if len(line) < 3: continue #skip empty lines
		if header == True:
			header = False #skip the header line
		else:
			spls = line.strip().split("\t")
			gene = spls[1]
			if gene == last_gene or block == []:
				block.append(spls)
			else:
				isoform_picked.append(pick_isoform(block))
				block = [spls]
			last_gene = gene
	isoform_picked.append(pick_isoform(block)) #process the last block
	infile.close
	
	print "Writing selected isoforms"
	infile = open(sys.argv[2],"r")
	outfile = open(sys.argv[2]+".exemplar","w")
	for seq in SeqIO.parse(infile,"fasta"):
		seqid = seq.id.split(" ")[0]
		if seqid in isoform_picked:
			outfile.write(">"+seqid+"\n"+str(seq.seq)+"\n")
	infile.close()
	outfile.close()
		
