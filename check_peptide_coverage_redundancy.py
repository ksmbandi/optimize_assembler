from Bio import SeqIO
import os,sys

"""
blastp output file format:
-outfmt '6 qseqid qlen sseqid slen frames pident nident length mismatch gapopen qstart qend sstart send evalue bitscore'

0-qseqid	1-qlen		2-sseqid	3-slen		4-frames	5-pident	6-nident
7-length	8-mismatch 	9-gapopen 	10-qstart	11-qend		12-sstart 	13-send
14-evalue	15-bitscore
"""

MKDB_CMD = "makeblastdb"
BLASTP_CMD = "blastp"
MIN_PIDEN = 50.0 #so paralogs are less likely to be counted

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "usage: python check_peptide_coverage_redundancy.py peptide_fasta_DIR peptide_fasta_file_ending reference_proteome_fasta"
		sys.exit(0)

	DIR = sys.argv[1]+"/"
	outfile = open(DIR+"reference_covarage","w")
	header = "file_name\ttotal_coverage(bp)\ttotal_coverage(num_genes)\t"
	header += "#assembled_to_>=60%\t#assembled_to_>=80%\tredundancy\n"
	outfile.write(header)
	file_ending = sys.argv[2]
	ref = sys.argv[3]
	"""
	#make a blast database with the reference proteome
	cmd = MKDB_CMD+" -in "+ref+" -parse_seqids -dbtype prot -out "+ref
	print cmd
	os.system(cmd)
	
	for i in os.listdir(DIR):
		if not i.endswith(file_ending): continue
		#blastp
		cmd = BLASTP_CMD+" -db "+ref+" -query "+DIR+i
		cmd += " -evalue 10 -num_threads 10 -max_target_seqs 1 -out "+DIR+i
		cmd += ".rawblastp -outfmt '6 qseqid qlen sseqid slen frames pident nident length mismatch gapopen qstart qend sstart send evalue bitscore'"
		print cmd
		os.system(cmd)
	"""
	
	for i in os.listdir(DIR):
		if i.endswith("blastp") or i.endswith("blastx"):
			with open(DIR+i,"r") as infile:
				Qlist = [] #list of all the query ids
				Tcov_dict = {} #key is gene name, value is [max T cov,max perc T cov]
				for line in infile:
					spls = line.strip().split("\t")
					piden = float(spls[5])
					if piden < MIN_PIDEN: continue
					Q,T = spls[0],spls[2]
					Tcov = float(spls[13])-float(spls[12])
					Tcov_perc = Tcov/float(spls[3])
					if T not in Tcov_dict:
						Tcov_dict[T] = [0,0.0]
					if Tcov_perc > Tcov_dict[T][1]:
						Tcov_dict[T][0] = Tcov
						Tcov_dict[T][1] = Tcov_perc
						#use the larger coverage; do not combine coverages
					Qlist.append(Q)
			
			num_genes = len(set(Qlist))
			total_cov = 0
			perc_80_count = 0 #number of genes assembled to >= 80% length
			perc_60_count = 0
			for gene in Tcov_dict:
				total_cov += Tcov_dict[gene][0]
				if Tcov_dict[gene][1] >= 0.8: perc_80_count += 1
				if Tcov_dict[gene][1] >= 0.6: perc_60_count += 1
			gene_covered = len(Tcov_dict)
			redundancy = float(num_genes) / gene_covered
			out = i+"\t"+str(total_cov)+"\t"+str(gene_covered)+"\t"
			out += str(perc_60_count)+"\t"+str(perc_80_count)+"\t"+str(redundancy)+"\n"
			outfile.write(out)
			
	outfile.close()
	print "Coverage statistics written to",DIR+"reference_covarage"
