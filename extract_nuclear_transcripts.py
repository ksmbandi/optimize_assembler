"""
Input: a folder of blat result files end with ".psl".
default input blat result file format:
col 0: match		--> float
col 1: mismatch		--> float
col 9: Q name
col 10: Q size		--> float
col 11: Q start		--> float
col 12: Q end		--> float

Outputs nuclear transcripts
"""

import sys,os
from Bio import SeqIO

SIMILARITY_CUTOFF = 0.95 #only consider HSPs >= this percentage similarity
Q_COV_CUTOFF = 0.8 #query transcripts >= this percentage merged is considered a positive hit

#take a block of blat output from one query
#return true if >= 80% of it has >=95% similarity to chl or mt genomes
def process_block(block):
	hit = block[0]
	size,start,end = hit[10],hit[11],hit[12]
	perc = abs(end - start) / size #not +1 since start starts from 0
	if len(block) == 1: return perc >= Q_COV_CUTOFF
	else:
		for hit in block[1:]:
			start = min(start,end,hit[11],hit[12])
			end = max(start,end,hit[11],hit[12])
		perc = (end - start) / size #not +1 since start starts from 0
		return perc >= Q_COV_CUTOFF

if __name__ == "__main__":
	if len(sys.argv) != 3:
		print "usage: python extract_nuclear_transcripts.py all_transcripts blat_output"
		sys.exit(0)
		
	infile = open(sys.argv[2],"rU")
	lines = infile.readlines()[5:]
	chl_mt = [] #store all transcripts that are either chloroplast or mitochondrial
	block = [] #store all blat output lines of one query
	last_query = ""
	
	for line in lines:
		if len(line) < 3: continue #ignore empty lines
		line = line.strip().split('\t')
		for column in [0,1,10,11,12,14,15,16]:
			line[column] = float(line[column])#str to float
		similarity = line[0] / (line[0]+line[1])
		#not +1 since start starts from 0
		if similarity >= SIMILARITY_CUTOFF:
			query = line[9]
			if query == last_query or block == []:
				block.append(line)#add to block	
			else: #send the whole block off
				if process_block(block):
					chl_mt.append(query)
				block = [line]#reset and initiate the block
			last_query = query
	
	#process the last block
	if process_block(block):
		chl_mt.append(query)	
	infile.close()
	
	input_handle = open(sys.argv[1],"rU")
	output_handle = open(sys.argv[1]+".nuclear","w")
	for seq in SeqIO.parse(input_handle,"fasta"):
		if seq.id not in chl_mt:
			SeqIO.write(seq,output_handle,"fasta")
	input_handle.close()
	output_handle.close()
