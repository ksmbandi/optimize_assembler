import sys
from Bio import SeqIO

"""
input RSEM.isoforms.results, trinity output
RSEM.isoforms.results columns:
0-transcript_id		1-gene_id	2-length	3-effective_length	
4-expected_count	5-TPM		6-FPKM		7-IsoPct

output filtered trinity output
remove the least covered isoform
If multiple isoforms have the same coverage, remove the shorter one
"""

def pick_isoform(block):
	if len(block) == 1:
		return [] #no isoform to remove
	else:
		transcript,length,IsoPct = block[0][0],float(block[0][2]),float(block[0][7])
		for i in block[1:]:
			perc = float(i[7])
			if perc < IsoPct or (perc == IsoPct and float(i[2]) < length):
				transcript,length,IsoPct = i[0],float(i[2]),perc
		return [transcript]

if __name__ == "__main__":
	if len(sys.argv) != 4:
		print "python filter_isoform_trinity_RSEM.py RSEMresults fasta_file outfile"
		sys.exit(0)
		
	infile = open(sys.argv[1],"r")
	header = True
	last_gene = ""
	block = [] #store all lines of one gene
	isoform_to_remopve = [] #store transcript_id to output
	print "Reading RSEM output"
	for line in infile:
		if len(line) < 3: continue #skip empty lines
		if header == True:
			header = False #skip the header line
		else:
			spls = line.strip().split("\t")
			gene = spls[1]
			if gene == last_gene or block == []:
				block.append(spls)
			else:
				isoform_to_remopve += pick_isoform(block)
				block = [spls]
			last_gene = gene
	isoform_to_remopve += pick_isoform(block) #process the last block
	infile.close
	
	print "Writing filtered isoforms"
	infile = open(sys.argv[2],"r")
	outfile = open(sys.argv[3],"w")
	for seq in SeqIO.parse(infile,"fasta"):
		seqid = seq.id.split(" ")[0]
		if seqid not in isoform_to_remopve:
			out = ">"+seqid+"\n"+str(seq.seq)+"\n"
			outfile.write(out)
	infile.close()
	outfile.close()
		
