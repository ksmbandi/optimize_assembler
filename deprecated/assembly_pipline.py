"""
Old pipeline used to process 1kp seqs in 2012
Change namestoprocess
Change the string read1 and read2 according to file names of the fastq raw reads
"""

import sys,os

namestoprocess = ["FVXD","GIWN","OLES","QAIR","WOBD","WPYJ","YNFJ"]
kmers = ["21","31","41","51","61"]
path_blast_database = "~/blast/eudicot"

if __name__ =="__main__":
	if len(sys.argv) != 2:
		print "usage: python assembly_pipeline.py DIR"
		sys.exit()
		
	DIR = sys.argv[1]
	for i in namestoprocess:
		read1 = DIR+"/"+i+"-read_1.fq"
		read2 = DIR+"/"+i+"-read_2.fq"
		
		#Remove low quality reads and adaptor contaminations
		com = "python ./clean_fastq.py "
		com += read1+" "+read2+" "+i+".cleaned.shuffled"
		print com
		os.system(com)
		
		for k in kmers:
			#assemble using Trinity/Oases
			assembledir = DIR+"/"+i+"_"+k+"_oases"
			com = "velveth "+assembledir+" "+k+" -shortPaired -fastq "
			com += DIR+"/"+i+".cleaned.shuffled"
			os.system(com)
			com = "velvetg "+assembledir+" -read_trkg yes"
			print com
			os.system(com)
			com = "oases "+assembledir+" -ins_length 200"
			print com
			os.system(com)
			
			#Remove large graph and sequence files to save space
			postassemblefilename = i+"_"+k+".exemplar.fa"
			com = "cd "+assembledir
			com += " && rm Graph2 PreGraph Roadmaps Sequences LastGraph"
			print com
			os.system(com)
			
			#Pick exemplars for each locus
			if int(k) < 60: TperL = "1"#set max # of transcripts per locus for small ks
			else: TperL = "50"#set max # of transcripts per locus for k = 61
			com = "python ~/optimize_assembler/process_oases_transcripts.py "
			com += assembledir+" "+TperL+" "+postassemblefilename
			print com
			os.system(com)
			
			#fix seq names 
			#from ">Locus_1_Transcript_1/6_Confidence_0.200_Length_325"
			#to ">k_i@L1_T1_6"
			com = "cd "+assembledir+" && sed -e 's/\//_/g' -e 's/_Confi.*//g' -e 's/>Locus_/>"
			com += i+"_"+k+"@L/g' -e 's/Transcript_/T/g' "
			com += postassemblefilename+" >../"+postassemblefilename
			print com
			os.system(com)
	
		#clean up intermediate sequence files to save some space
		os.system("rm "+read1+"thinned")
		os.system("rm "+read2+"thinned")
		com = "tar -cjf "+DIR+i+".reads.tar.bz2 "+i+"*.fq"
		print com
		os.system(com)
		
		#cap3 to merge multiple kmer assemblies
		com = "cat "+DIR+i+"_[0-9][0-9].exemplar.fa >"+i+".exemplar.fa"
		print com
		os.system(com)
		com = "cap3 "+DIR+i+".exemplar.fa -o 200 -p 99"
		print com
		os.system(com)
		com = "sed -i -e 's/>/>"+DIR+i+"@/g' "+i+".exemplar.fa.cap.contigs"
		print com
		os.system(com)
		com = "cat "+DIR+i+".exemplar.fa.cap.contigs "+DIR+i+".exemplar.fa.cap.singlets >"+DIR+i+".cap.fa"
		print com
		os.system(com)
		
		#blastx to find trans chimeras
		cmd = "blastx -db "+path_blast_database+" -query "+DIR+i+".cap.fa "
		cmd += "-evalue 0.01 -outfmt "
		cmd += "'6 qseqid qlen sseqid slen frames pident nident length mismatch gapopen qstart qend sstart send evalue bitscore' "
		cmd += "-out "+DIR+i+"blastx -num_threads=10 -max_target_seqs 100"
		print com
		os.system(com)
		
		#detect chimera and cut
			
