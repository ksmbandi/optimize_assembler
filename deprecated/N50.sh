awk -F "\t" '{print $4}' PAZJ_19_transcripts.stats | sort -n | awk 'BEGIN {sum=0} {sum += $1; print $1, sum}' | tac - | awk 'NR==1 {halftot=$2/2} lastsize>halftot && $2<halftot {print} {lastsize=$2}'

