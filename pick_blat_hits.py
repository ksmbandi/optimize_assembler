"""
Input: a folder of blat result files end with ".psl".
default input blat result file format:
0: match		1: mismatch		2: rep. match	3: N's			4: Q gap count
5: Q gap bases	6: T gap count	7: T gap bases	8: strand		9: Q name**
10: Q size		11: Q start		12: Q end		13: T name		14: T size
15: T start		16: T end		17: block count	18: block sizes 19: Q starts
20: T starts 

Output:
- a file named "chimera_stats" summarizing types and numbers of chimera
- a ".goodhits" file with the best hit for each query
  columns added:	21: perc_Q_cov		22: perc_T_cov
- a ".probhits" file with blat results indicating chimeric query
"""

import sys,os

#for DNA of the same species:
SIMILARITY_CUTOFF = 0.95 #only consider HSPs >= this percentage similarity
MATCH_CUTOFF = 100 #only consider HSPs with # matches >= this

#for amino acids between closely-related species:
#SIMILARITY_CUTOFF = 0.9 #only consider HSPs >= this percentage similarity
#MATCH_CUTOFF = 50 #only consider HSPs with # matches >= this

PERC_OVERLAP_CUTOFF = 0.2 #separated if two Q_cov overlap < this % of the shorter one

def separated(start1,end1,start2,end2):
	"""
	take start and end positions from two query coverages
	if they overlap less than 20% of the shorter query coverage
	and less than 60 bp, they are separated
	"""
	length1,length2 = abs(end1-start1),abs(end2-start2) #do not +1
	start,end = min(start1,end1,start2,end2),max(start1,end1,start2,end2)
	overlap = (length1+length2) - (end-start)
	#overlap can be negative values but only the upper limit matters
	if overlap < min(PERC_OVERLAP_CUTOFF*min(length1,length2), 60):
		return True #label as separated
	return False


def print_good(hit):
	"""output the best hit to the ".goodhits" file"""
	for i in hit:
		outfile1.write(str(i)+"\t")
	perc_Q_cov = abs(hit[12]-hit[11]) / hit[10] #do not +1
	perc_T_cov = abs(hit[16]-hit[15]) / hit[14] #do not +1
	outfile1.write(str(perc_Q_cov)+"\t"+str(perc_T_cov)+"\n")
	if perc_T_cov >= 0.8:
		longer80 += 1
	else:
		shorter80 += 1

def print_chimera(block):
	"""output entire block to the ".probhits" file"""
	for i in block:
		for j in i:
			outfile2.write(str(j)+"\t")
		outfile2.write("\n")

def process_block(block):
	"""take a block of all blat output lines for one query and check"""
	blat_hits += 1 #seq stats
	#if there's only one hit, then that's the best hit
	if len(block) == 0:
		pass #no line passed the filters. Ignore the query
	elif len(block) == 1:
		print_good(block[0])
	else: #multiple blat his
		best_hit = block[0]
		for hit in block[1:]: #find the best hit with max matching nucleotides
			if hit[0] > best_hit[0]: best_hit = hit
		for hit in block: #go through the block again
			if separated(best_hit[11],best_hit[12],hit[11],hit[12]):
				chimera += 1 #chimera detected
				same_strand = (best_hit[8] == hit[8]) #8: strand
				same_T = (best_hit[13] == hit[13]) #13: target name
				if same_strand and same_T:
					cis_self += 1
				elif same_strand and not same_T:
					cis_multi += 1
				elif not same_strand and same_T:
					trans_self += 1
				else:
					trans_multi += 1
				print_chimera(block)
				return
		print_good(best_hit) #didn't find chimera
		

if __name__ == "__main__":
	if len(sys.argv) != 2:
		print "usage: pick_blat_hits.py DIR_blat_output"
		sys.exit(0)
	DIR = sys.argv[1]+"/"
	outfile3 = open(DIR+"chimera_stats","w")
	header = "file\t"
	header += "total_hits\t>80%_cov\tgood<80%_cov\t"
	header += "chimera\tcis_self\tcis_multi\ttrans_self\ttrans_multi\n"
	outfile3.write(header)
	
	#loop through all .psl files in directory
	#file names look like prefix_k_package.sizefilter.fa.psl
	for psl_file in os.listdir(DIR):
		if psl_file[-4:] != ".psl": continue
		print "Processing file "+psl_file
		infile = open(DIR+psl_file,"rU")
		blat_output_lines = infile.readlines()[5:]
		outfile1 = open(DIR+psl_file[:-4]+".good","w")
		outfile2 = open(DIR+psl_file[:-4]+".chimera","w")
		
		block = [] #store all blat output lines of one query
		last_query = ""		
		#initicate counters
		blat_hits,longer80,shorter80,chimera = 0,0,0,0
		cis_self,cis_multi,trans_self,trans_multi = 0,0,0,0
		
		for line in blat_output_lines:	
			#Parse data
			if len(line) < 3: continue #ignore empty lines
			spls = line.strip().split('\t')
			for column in [0,1,10,11,12,14,15,16]:
				spls[column] = float(spls[column]) #fix data type
			similarity = spls[0] / (spls[1]+spls[0])
			match = spls[0]
			query = spls[9]
			
			#Get blocks with the same query
			if similarity >= SIMILARITY_CUTOFF and match >= MATCH_CUTOFF:
				if query == last_query or block == []:
					block.append(spls)#add to block	
				else:
					process_block(block)#send the whole block off
					block = [spls]#reset and initiate the block						
				last_query = query
				
		process_block(block)#process the last block
				
		infile.close()
		outfile1.close()
		outfile2.close()
		
		#log stats for each blat output file
		out = psl_file+"\t"
		out += str(blat_hits)+"\t"+str(longer80)+"\t"+str(shorter80)+"\t"+str(chimera)+"\t"
		out += str(cis_self)+"\t"+str(cis_multi)+"\t"+str(trans_self)+"\t"+str(trans_multi)+"\n"
		outfile3.write(out)
	outfile3.close()
